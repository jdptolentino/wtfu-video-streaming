<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// User Login and Register
Route::post('login', 'API\UserController@login');
Route::post('login/token', 'API\UserController@loginWithAuthToken');
Route::post('register', 'API\UserController@register');
Route::get('verify/email/{token}', 'API\UserController@verifyEmail');

Route::post('/payment', ['as' => 'payment', 'uses' => 'PaymentController@payWithpaypal']);
Route::get('/payment/status',['as' => 'status', 'uses' => 'PaymentController@getPaymentStatus']);

// Password Endpoints
Route::group([
    'namespace' => 'Auth',
    'middleware' => 'api',
    'prefix' => 'password'
], function () {
    Route::post('create', 'PasswordResetController@create');
    Route::get('find/{token}', 'PasswordResetController@find');
    Route::post('reset', 'PasswordResetController@reset');
});

// Site Endpoints
Route::group([
    'middleware' => 'auth:api',
    'namespace' => 'API',
], function(){
    // User
    Route::get('check-user', 'UserController@checkUser');
    Route::post('user/profile' , 'UserController@details');
    Route::post('user/{id}/update' , 'UserController@update');

    // Videos
    Route::get('videos', 'VideosController@all');
    Route::get('videos/{id}', 'VideosController@getById');
    Route::get('videos/category/{name}', 'VideosController@getByCategory');

    // Series
    Route::get('series', 'SeriesController@all');
    Route::get('series/{slug}', 'SeriesController@getSeriesBySlug');
    Route::get('search', 'SeriesController@search');

    // Transactions
    Route::get('transactions', 'TransactionController@getAll');

    // Subscriptions
    Route::group(['prefix' => 'subscription'], function() {
        Route::get('/', 'SubscriptionController@getAll');
        Route::post('/choose-plan', 'SubscriptionController@addPlan');
        Route::post('/change-plan/{subscription}', 'SubscriptionController@changePlan');
        Route::post('{id}/cancel', 'SubscriptionController@cancel');
    });

});
