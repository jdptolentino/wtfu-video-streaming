<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AccessGranted extends Notification
{
    use Queueable;

    protected $user;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = env('WEB_APP_URL') . 'login?token1='. $this->user->activation_token;

        $item = $this->user->latestTransaction[0];
        $txnDate = strtotime($item->created_at);
        $startDate = date('M d, Y', $txnDate);
        $endDate = date('M d, Y', strtotime("+" . $item->plan->frequency . " " . $this->frequencyType($item->plan->frequency_type), $txnDate));
       
        return (new MailMessage)
                    ->subject('Premium Access Activation')
                    ->greeting(' ')
                    ->line('Congratulations!')
                    ->line('You now have premium access on these dates:')
                    ->line('START: '.$startDate)
                    ->line('END: '.$endDate)
                    ->line('Thank you!')
                    ->salutation('Video Platform Team');
    }

    /**
     * Get Plan frequency
     */
    private function frequencyType($type) {

        switch ($type) {
            case $type == 'DAY':
                return 'day';
            case $type == 'MONTH':
                return 'month';
            case $type == 'YEAR':
                return 'year';
            default:
                return 'month';
        }

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
