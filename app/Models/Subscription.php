<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subscription extends Model
{

    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subscriptions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'plan_id', 'status'];

    /**
     * Relationships
     */
    public function plan(){
        return $this->hasOne(\App\Models\SubscriptionPlan::class, 'id', 'plan_id');
    }

    public function user(){
        return $this->hasOne(\App\User::class, 'id', 'user_id');
    }

    /**
    * Functions
    */
    public static function applySubs($user, $id) {
        if ($user) {
            $subscription = new self;

            $subscription->user_id = $user->id;
            $subscription->plan_id = $id;
            $subscription->save();

        }

    }

    public static function changePlan($user, $id, $subscription) {
        if ($user) {
            $subscription = self::where('id', $subscription)->where('user_id', $user->id)->first();
            $subscription->plan_id = $id;
            $subscription->save();
        }

    }

    public static function checkIfUserHasRequestedPlan($user){
        if ($user) {
            return Subscription::where('user_id', $user->id)->where('status', 'REQUESTED')->count();
        }
    }
}
