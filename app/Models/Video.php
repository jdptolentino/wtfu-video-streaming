<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;

    // this is a recommended way to declare event handlers
    public static function boot() {
        parent::boot();

        static::deleting(function($video) { // before delete() method call this
                $video->videoInSeries()->delete();
                // do the rest of the cleanup...
        });
    }

    protected $table = 'videos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'link', 'description', 'category_id', 'type', 'photo', 'isEmbed'
    ];

    protected $hidden = [
        'category_id'
    ];
    protected $appends = array('thumbnail');

    /**
     * Mutator
     * Video thumbnail
     */
    public function getThumbnailAttribute() {
        // $videoArr = explode('/', $this->link);
        // if (sizeOf($videoArr) > 0) {
        //     $url = "https://vimeo.com/api/v2/video/" . $videoArr[count($videoArr) - 1] . ".json";
            
        //     $ch = curl_init();

        //     // set url
        //     curl_setopt($ch, CURLOPT_URL, $url);

        //     //return the transfer as a string
        //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        //     // $output contains the output string
        //     $output = curl_exec($ch);

        //     // close curl resource to free up system resources
        //     curl_close($ch);     

        //     $data = json_decode($output);
    
        //     return is_array($data) ? $data[0]->thumbnail_large : 'http://via.placeholder.com/350x300.png?text=Sorry%20thumbnail%20not%20available';
        // }
        if (isset($this->photo)) {
            return env('APP_URL') . 'uploads/' . $this->photo;
        }
        return "http://via.placeholder.com/350x300.png?text=Sorry%20thumbnail%20not%20available";
    }

    public function category() {
        return $this->hasOne(\App\Models\Category::class, 'id', 'category_id');
    }

    public function videoInSeries() {
        return $this->hasMany(\App\Models\SeriesVideos::class, 'video_id', 'id');
    }
}
