<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class UsersExport implements FromCollection, WithHeadings
{
    use Exportable;

    protected $dataArray;

    function __construct($dataArray=[]){
        $this->dataArray = $dataArray;
    }
    
    public function collection()
    {
        $data = [];

        if($this->dataArray){
            foreach ($this->dataArray as $key => $value) {
                $data[] = [
                    'Name' => $value->full_name,
                    'Email' => $value->email,
                    'Status' => $value->status
                ];
            }
            $data[] = [
                'Name' => '',
                'Email' => '',
                'Status' => ''
            ];
        }
        return collect($data); 
    }

    public function headings(): array
    {
        return [
            'Name',
            'Email',
            'Status'
        ];
    }  
}