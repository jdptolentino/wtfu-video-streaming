<?php

namespace App\Exports;

use App\Models\Subscription;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\Exportable;

class RequestsExport implements FromCollection, WithHeadings
{
    use Exportable;

    protected $dataArray;

    function __construct($dataArray=[]){
        $this->dataArray = $dataArray;
    }
    
    public function collection()
    {
        $data = [];

        if($this->dataArray){
            foreach ($this->dataArray as $key => $value) {
                $data[] = [
                    'Username' => $value->user->user_name,
                    'Email' => $value->user->email,
                    'Subscription' => $value->plan->title,
                    'Amount' => number_format($value->plan->amount,2),
                    'Status' => $value->status
                ];
            }
            $data[] = [
                'Username' => '',
                'Email' => '',
                'Subscription' => '',
                'Amount' => '',
                'Status' => ''
            ];
        }
        return collect($data); 
    }

    public function headings(): array
    {
        return [
            'Username',
            'Email',
            'Subscription',
            'Amount',
            'Status'
        ];
    }  
}