<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MiscController extends Controller
{
    public function thumbnail(Request $requests) {
        $id = $requests->id;

        $url = "https://vimeo.com/api/v2/video/" . $id . ".json";
        
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);     

        $data = json_decode($output);

        return response()->json($data);
    }
}
