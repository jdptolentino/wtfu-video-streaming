<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Subscription;
use App\Exports\RequestsExport;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class ReportController extends Controller
{
    public function index(Request $request){
        
        $perPage = 25;
        $status = '';
        $date_range = '';

        $subscription = [];
        $user = [];
        $tab = isset($_GET['tab'])?$_GET['tab']:'requests';

        if($request->isMethod('post')){
            if($request->tab == 'users' || $tab == 'users'){
                $tab = 'users';
                $status = $request->status;

                $user = User::where('status',$status);
    
                $user = $user->latest()->paginate($perPage);
                
                $subscription = Subscription::with(['plan', 'user'])->latest()->paginate($perPage);
            }else{
                $tab = 'requests';
                $status = $request->status;
                $date_range = $request->date_range;
    
                $dateArr = explode("-", $request->date_range, 2);
                $start = Carbon::parse($dateArr[0])->startOfDay();
                $end = Carbon::parse($dateArr[1])->endOfDay();
    
                $subscription = Subscription::with(['plan', 'user']);
                
                $subscription = $subscription->where('status',$status);
    
                $subscription = $subscription->whereBetween('created_at',[$start, $end]);
    
                $subscription = $subscription->latest()->paginate($perPage);
                $user = User::latest()->paginate($perPage);
            }
        }else{
            $subscription = Subscription::with(['plan', 'user'])->latest()->paginate($perPage);
            $user = User::latest()->paginate($perPage);
        }

        return view('reports.index', compact('subscription','user', 'tab', 'status', 'date_range'));
    }
    
    public function exportRequests(Request $request) 
    {
        $status = $request->status;
        $date_range = $request->date_range;

        $dateArr = explode("-", $request->date_range, 2);
        $start = Carbon::parse($dateArr[0])->startOfDay();
        $end = Carbon::parse($dateArr[1])->endOfDay();
        
        $subscriptions = Subscription::with(['plan', 'user']);
        
        $subscriptions = $subscriptions->where('status',$status);

        $subscriptions = $subscriptions->whereBetween('created_at',[$start, $end]);

        $subscriptions = $subscriptions->latest()->get();

        return Excel::download(new RequestsExport($subscriptions), 'requests.xlsx');
    }

    public function exportUsers(Request $request) 
    {
        $status = $request->status;
        
        $users = User::where('status',$status);
    
        $users = $users->latest()->get();

        return Excel::download(new UsersExport($users), 'users.xlsx');
    }
}