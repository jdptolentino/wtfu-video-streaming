<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Subscription;
use App\Models\SubscriptionPlan;
use App\User;
use App\Notifications\PaymentInstruction;

class SubscriptionController extends Controller
{

    private $user;
    public function __construct(Request $request) {
        if ($request->header('x-user-id')) {
            $this->user = $request->header('x-user-id');
        }
    }
    /**
     * Cancel Subscription
     */
    public function cancel($id) {
        $subscription = Subscription::find($id);

        $subscription->status = 'CANCELLED';
        $subscription->save();

        if($subscription) {
            return response()->json([
                'message' => 'Subscription successfully cancelled ',
                'error' => false
            ], 200);
        }

        return response()->json([
            'message' => 'User not found',
            'error' => true
        ], 400);
    }

    /**
     * Get Subscription Plan
     */
    public function getAll() {
        $subscription = SubscriptionPlan::where('status', 'ENABLED')->get();

        return response()->json([
            "data" => $subscription,
        ], 200);
    }

    /**
     * Apply for Subscription Plan
     */
    public function addPlan(Request $request) {
        if ($this->user && $request->has('plan')) {

            $user = User::find($this->user);
            if($user) {
                if (!Subscription::checkIfUserHasRequestedPlan($user)) {
                   Subscription::applySubs($user, $request->plan);
                    $user->notify(
                        new PaymentInstruction( SubscriptionPlan::find($request->plan))
                    );
                } else {
                    return response()->json([
                        "message" => 'Subscription already requested!',
                    ], 400);

                } 
            }


            return response()->json([
                "message" => 'Success! Please check email your email.',
            ], 200);
        }


        return response()->json([
            "message" => 'Unknown user',
        ], 400);
    }

    /**
     * Change Subscription Plan
     */
    public function changePlan(Request $request, $subscription) {
        if ($this->user && $request->has('plan')) {

            $user = User::find($this->user);
            if($user) {
                Subscription::changePlan($user, $request->plan, $subscription);
            }
            return response()->json([
                "message" => 'Subscription changed successfully',
            ], 200);
        }


        return response()->json([
            "message" => 'Unknown user',
        ], 400);
    }
}
