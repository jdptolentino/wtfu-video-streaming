<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Notifications\VerifyEmail;
use App\User;

use Validator;
class UserController extends Controller
{
    public $successStatus = 200;

    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){

        $checkUser = User::with(['plan', 'subscription'])->where('email', request('email'))->first();

        if ($checkUser) {
            if (Hash::check(request('password'), $checkUser->password)) {
                if ($checkUser->status === 'EMAIL_VERIFICATION') {
                    return response()->json([
                        'message'=>'Email not yet verified',
                        'error'=>'false'
                    ], 401);
                }
                $user = $checkUser;
                $success['token'] =  $user->createToken('VideoStreaming')->accessToken;
                $success['user'] = $user;

                return response()->json( $success, $this->successStatus);
            }
        }
        return response()->json([
            'message' => 'Username or Password is invalid',
            'error' => true
        ], 401);
    }

    /**
     * login with activation token
     *
     * @return \Illuminate\Http\Response
     */
    public function loginWithAuthToken(){

        $checkUser = User::with(['plan', 'subscription'])->where('activation_token', request('token'))->first();

        if ($checkUser) {
            if ($checkUser->status === 'EMAIL_VERIFICATION') {
                return response()->json([
                    'message'=>'Email not yet verified',
                    'error'=>'false'
                ], 401);
            }
            $user = $checkUser;
            $success['token'] =  $user->createToken('VideoStreaming')->accessToken;
            $success['user'] = $user;

            return response()->json( $success, $this->successStatus);
        }
        return response()->json([
            'message' => 'Auth token is invalid',
            'error' => true
        ], 401);
    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $response = new \stdClass;
        $response->message = 'User created and waiting for email verification';
        $response->error = false;

        $messages = [
            'unique' => 'The :attribute has already been taken.',
            'email' => 'The :attribute has already been taken.'
        ];

        
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required',
        ], $messages);

        if ($validator->fails()) {
            $response->message = $validator->errors();
            return response()->json($response, 400);
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['activation_token'] = Str::random(60);
        $user = User::create($input);

        if (!$user) {
            if (!$user) {
                $user->message = $user;
            }

            $user->error = true;
        } else {
             $user->notify(
                new VerifyEmail($user->activation_token)
            );
        }
        return response()->json($user);

    }

    /**
     * Forgot password
     */

    public function verifyEmail() {
        $response = [
            'message' => '',
            'error' => true,
            'code' => 400
        ];

        $user = User::where('activation_token', request('token'))->first();

        if ($user) {

            switch ($user->status) {
                case "ACTIVE":
                    $response['message'] = 'Email is already verified';
                break;
                case "DELETE":
                    $response['message'] = 'Connected email has been deleted';
                break;
                case "SUSPEND":
                    $response['message'] = 'Connected email is suspended';
                break;
                case "EMAIL_VERIFICATION":
                    $user->status = 'active';
                    $user->save();

                    $response['message'] = "Email has been verified";
                    $response['token'] = $user->activation_token;
                    $response['error'] = false;
                    $response['code'] = 200;
                break;
            }

        } else {
            $response['message'] = "Error occured. Please contact support.";
            $response['error'] = true;
        }

        return response()->json($response, $response['code']);

    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details(Request $request)
    {
        $user = User::with(['subscription'])->find($request->header('x-user-id'));
        if ($user) {
            return response()->json([ 'data' => $user], $this->successStatus);
        }
        return response()->json([ 'message' => 'User not found', 'error' => true], 400);
    }

    /**
     * Check user if Authenticated
     *
     * @return \Illuminate\Http\Response
     */
    public function checkUser(Request $request)
    {
        $user = User::with(['plan', 'subscription'])->find($request->header('x-user-id'));
        if ($user) {
            return response()->json(['authenticated' => true, 'user' => $user, 'token'=>$user->createToken('VideoStreaming')->accessToken], $this->successStatus);

        }
        return response()->json(['authenticated' => true], $this->successStatus);
    }

    /**
     * Update user
     */
    public function update($id, Request $request) {
        $response = new \stdClass;
        $response->message = '';
        $response->error = false;

        $messages = [
            'min' => 'The :attribute must be minimum of :min alphanumeric characters',
            'unique' => 'The :attribute has already been taken.'
        ];

        $user = User::find($id);
        if ($user) {
            $validator = Validator::make($request->all(), [
                // 'user_name' => 'required|string|alpha_num|min:8|max:255|unique:users,user_name,'.$id,
            ], $messages);
    
            if ($validator->fails()) {
                $response->message = $validator->errors();
                return response()->json($response, 400);
            }

            $user->fill($request->all());

            $user->save();

            return response()->json([ 'data' => $user], $this->successStatus);
        }
        return response()->json([ 'message' => 'User not found', 'error' => true], 400);
    }

}
