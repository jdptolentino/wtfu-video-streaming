<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Video;
use App\Models\Category;

class VideosController extends Controller
{
    /**
     * Get all Videos
     */
    public function all(Request $request) {
        $perPage = $request->has('perPage') ? $request->perPage : 10;
        $page = $request->has('page') ? $request->page : 1;
        $skip = ($page - 1) * $perPage;
        $videos = Video::with('category')->where('status', 'ENABLED');

        return response()->json([
            "total" => $videos->count(),
            "perPage" => $perPage,
            "page" => $page,
            "data" => $videos->skip($skip)->take($perPage)->get(),
        ], 200);

    }

    /**
     * Get video by ID
     * @param $id
     */
    public function getById($id) {
        $videos = Video::find($id);

        if ($videos) {
            return response()->json([
                "data" => $videos ? $videos : [],
            ], 200);
        }

        return response()->json([
            "message" => 'Video not found.'
        ], 400);
    }

    public function getByCategory(Request $request, $name) {
        $perPage = $request->has('perPage') ? $request->perPage : 10;
        $page = $request->has('page') ? $request->page : 1;
        $skip = ($page - 1) * $perPage;

        $categoryName = str_replace('-', ' ', $name);
        $category = Category::where('name', $categoryName)->first();

        if ($category) {
            $videos = Video::with('category')->where('category_id', $category->id);

            return response()->json([
                "total" => $videos->count(),
                "perPage" => $perPage,
                "page" => $page,
                "data" => $videos->skip($skip)->take($perPage)->get(),
            ], 200);
        }

        return response()->json([
            "message" => "Category not found"
        ], 400);

    }
}
