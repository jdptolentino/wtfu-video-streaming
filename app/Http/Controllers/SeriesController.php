<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Series;
use App\Models\Video;
use App\Models\SeriesVideos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SeriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $series = Series::latest()->paginate($perPage);
        } else {
            $series = Series::latest()->paginate($perPage);
        }

        return view('series.index', compact('series'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('series.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        $validator = Validator::make($requestData, [
            'title' => 'required|string|max:255|unique:series',
            'description' => 'required',
            'slug' => 'required|string|unique:series',
            'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if ($validator->fails()) {
            return redirect('series/create')
            ->withInput()
            ->withErrors($validator);
        }

        $imageName = time().'.'.$request->photo->getClientOriginalExtension();
        $requestData['photo'] = $imageName;

        Series::create($requestData);

        $request->photo->move(public_path('images/series'), $imageName);

        return redirect('series')->with('flash_message', 'Series added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $series = Series::findOrFail($id);

        $selectedVideos = SeriesVideos::with('video')->where('series_id', $id)->get()->pluck('video');

        return view('series.show', compact('series', 'selectedVideos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $selectedVideos = [];
        $series = Series::findOrFail($id);
        $videos = Video::all();

        if ($series) {
            $selectedVideos = SeriesVideos::with('video')->where('series_id', $id)->get()->pluck('video');
            if ($selectedVideos) {
                $seriesVideos = $selectedVideos->pluck('id')->toArray();
                $videos = Video::whereNotIn('id', $seriesVideos)->get();
            }
        }
        return view('series.edit', compact('series', 'videos', 'selectedVideos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {

        $requestData = $request->all();

        if (!$request->has('videos_edit')) {
            $validator = Validator::make($requestData, [
                'title' => 'required|string|max:255|unique:series,title,'.$id,
                'description' => 'required',
                'slug' => 'required|string|unique:series,slug,'.$id,
                'photo' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ]);
    
            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator)->withInput($request->all());
            }
            
            $series = Series::findOrFail($id);
            if ($request->photo) {
                $imageName = time().'.'.$request->photo->getClientOriginalExtension();
                $requestData['photo'] = $imageName;
                $request->photo->move(public_path('images/series'), $imageName);
            }

            $series->update($requestData);

        } else {
            $newVideos = explode(',', $requestData['videos']);
            // Link videos to series
            if ($newVideos) {
                // Delete linked videos
                SeriesVideos::where('series_id', $id)->delete();

                if ($newVideos) {
                    // Insert new videos
                    foreach($newVideos as $i) {
                        if($i !== '') {
                            SeriesVideos::create([
                                'series_id' => $id,
                                'video_id' => $i
                            ]);
                        }
                    }
                }
            }
        }

        return redirect('series')->with('flash_message', 'Series details updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Series::destroy($id);

        return redirect('series')->with('flash_message', 'Series deleted!');
    }

    public function reorder() {
        $series = $series = Series::where('status', 'ENABLED')->orderBy('order','ASC')->get();
        return view('series.reorder', compact('series'));
    }

    public function reorderSave(Request $request) {
        $seriesIdArr = explode(',', $request['series']);

        $i = 1;
        foreach ($seriesIdArr as $id) {
            $series = Series::find($id);

            $series->order = $i;
            $series->save();
            
            $i++;
        }
        return redirect('series')->with('flash_message', 'Series order updated!');
    }
}
