<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\AdminTable as Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('table_search');
        $perPage = 25;

        if (!empty($keyword)) {
            $admin = Admin::where('id', '!=', \Auth::user()->id)
            ->where('status', '!=', 'DELETED')
            ->searchNameEmail($keyword)
            ->latest()->paginate($perPage);
        } else {
            $admin = Admin::where('id', '!=', \Auth::user()->id)->where('status', '!=', 'DELETED')->latest()->paginate($perPage);
        }

        return view('admin.index', compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'first_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('admin/create')
            ->withInput()
            ->withErrors($validator);
        }

        $requestData['password'] = Hash::make($requestData['password']);
        $requestData['activation_token'] = Str::random(60);

        Admin::create($requestData);

        return redirect('admin')->with('flash_message', 'Admin has been added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $admin = Admin::findOrFail($id);

        if ($admin) {
            if ($admin->id == \Auth::user()->id) {
                return redirect('admin');
            }
        }

        return view('admin.show', compact('admin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $admin = Admin::findOrFail($id);

        if ($admin) {
            if ($admin->id == \Auth::user()->id) {
                return redirect('admin');
            }
        }

        return view('admin.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'first_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins,email,'.$id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput($request->all());
        }


        $admin = Admin::findOrFail($id);
        $admin->update($requestData);

        return redirect('admin')->with('flash_message', 'Admin details has been updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        if ($id == \Auth::user()->id) {
            return redirect('admin');
        }
        
        Admin::destroy($id);

        return redirect('admin')->with('flash_message', 'Admin has been deleted!');
    }


    /**
     * Admin profile
     */
    public function profile(){
        $admin = \Auth::user();

        return view('admin.profile.show', compact('admin'));
    }

        /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function profileEdit()
    {
        $admin = \Auth::user();

        return view('admin.profile.edit', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function profileUpdate(Request $request)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'first_name' => 'required|string|max:255',
            'middle_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admins,email,'.\Auth::user()->id,
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }


        $admin = Admin::findOrFail(\Auth::user()->id);
        $admin->update($requestData);

        return redirect('profile')->with('flash_message', 'Admin details has been updated!');
    }

     /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function profilePasswordUpdate(Request $request)
    {
        
        $requestData = $request->all();
        
        $validator = Validator::make($requestData, [
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $admin = Admin::findOrFail(\Auth::user()->id);
        $admin->update(['password' => Hash::make($request->password)]);

        return redirect('profile')->with('flash_message', 'Admin password has been updated!');
    }

}
