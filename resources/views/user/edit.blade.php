@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
            <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Edit User</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>

        <section class="content">
            <div class="row px-2">
    
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Edit User</div>
                        <div class="card-body">
                            <a href="{{ url('/user') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <br />
                            <br />
    
                            @if ($errors->any())
                                <ul class="alert alert-danger">
                                    @foreach ($errors->all() as $error)
                                        <li class="ml-2">{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
    
                            <form method="POST" action="{{ url('/user/' . $user->id) }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}
    
                                @include ('user.form', ['formMode' => 'edit'])
    
                            </form>
    
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
