<div class="form-group {{ $errors->has('user_name') ? 'has-error' : ''}}">
    <label for="user_name" class="control-label">{{ 'Username' }}</label>
    <input class="form-control" name="user_name" type="text" id="user_name" value="{{ isset($user->user_name) ? $user->user_name : old('user_name')}}" >
    {!! $errors->first('user_name', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    <label for="email" class="control-label">{{ 'Email' }}</label>
    <input class="form-control" name="email" type="text" id="email" value="{{ isset($user->email) ? $user->email : old('email')}}" autocomplete="off">
    {!! $errors->first('email', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('first_name') ? 'has-error' : ''}}">
    <label for="first_name" class="control-label">{{ 'First Name' }}</label>
    <input class="form-control" name="first_name" type="text" id="first_name" value="{{ isset($user->first_name) ? $user->first_name : old('first_name') }}" >
    {!! $errors->first('first_name', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('middle_name') ? 'has-error' : ''}}">
    <label for="middle_name" class="control-label">{{ 'Middle Name' }}</label>
    <input class="form-control" name="middle_name" type="text" id="middle_name" value="{{ isset($user->middle_name) ? $user->middle_name : old('middle_name')}}" >
    {!! $errors->first('middle_name', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('last_name') ? 'has-error' : ''}}">
    <label for="last_name" class="control-label">{{ 'Last Name' }}</label>
    <input class="form-control" name="last_name" type="text" id="last_name" value="{{ isset($user->last_name) ? $user->last_name : old('last_name')}}" >
    {!! $errors->first('last_name', '<p class="text-danger help-block">:message</p>') !!}
</div>

@if (!isset($user->password))
    <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
        <label for="password" class="control-label">{{ 'Password' }}</label>
        <input class="form-control" name="password" type="password" id="password" value="{{ isset($user->password) ? $user->password : ''}}" autocomplete="off">
        {!! $errors->first('password', '<p class="text-danger help-block">:message</p>') !!}
    </div>
    <div class="form-group">
        <label for="c_password" class="control-label">{{ 'Confirm Password' }}</label>
        <input class="form-control" name="password_confirmation" type="password" id="c_password"  autocomplete="off">
    </div>
@endif

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select class="form-control" name="status" id="status">
        @if (isset($user->status))
            <option {{ $user->status == 'ACTIVE' ? 'selected': ''}} value="ACTIVE">ACTIVE</option>
            <option {{ $user->status == 'DELETED' ? 'selected': ''}} value="DELETED">DELETED</option>
            <option {{ $user->status == 'SUSPEND' ? 'selected': ''}} value="SUSPEND">SUSPEND</option>
        @else
            <option value="ACTIVE">ACTIVE</option>
            <option value="DELETED">DELETED</option>
            <option value="SUSPEND">SUSPEND</option>
        @endif
        </select>
    {!! $errors->first('status', '<p class="text-danger help-block">:message</p>') !!}
</div>

<!-- <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Password' }}</label>
    <input class="form-control" name="password" type="text" id="password" value="{{ isset($user->password) ? $user->password : ''}}" >
    {!! $errors->first('password', '<p class="text-danger help-block">:message</p>') !!}
</div> -->


<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
