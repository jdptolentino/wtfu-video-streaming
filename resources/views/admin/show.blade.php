@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">View Admin</h1>
                    </div>
                </div> 
            </div>
        </div>

        <section class="content">
            <div class="row px-2">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">{{ $admin->first_name }} {{ $admin->middle_name }} {{ $admin->last_name }}</div>
                        <div class="card-body">

                            <a href="{{ url('/admin') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <a href="{{ url('/admin/' . $admin->id . '/edit') }}" title="Edit admin"><button class="btn btn-primary btn-sm"><i class="fa fa-edit aria-hidden="true"></i> Edit</button></a>

                            <form method="POST" action="{{ url('admin' . '/' . $admin->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete admin" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                            </form>
                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th width="50%">ID</th><td>{{ $admin->id }}</td>
                                        </tr>
                                        <tr><th> First Name </th><td> {{ $admin->first_name }} </td></tr><tr><th> Middle Name </th><td> {{ $admin->middle_name }} </td></tr><tr><th> Last Name </th><td> {{ $admin->last_name }} </td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
