
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    <label for="password" class="control-label">{{ 'Password' }}</label>
    <input class="form-control" name="password" type="password" id="password" value="" autocomplete="off">
    {!! $errors->first('password', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group">
    <label for="c_password" class="control-label">{{ 'Confirm Password' }}</label>
    <input class="form-control" name="password_confirmation" type="password" id="c_password"  autocomplete="off">
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit-password' ? 'Update' : 'Create' }}">
</div>
