@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Edit Profile</h1>
                    </div>
                </div>
            </div>
        </div>

        <section class="content">
            <div class="row px-2">

                @if ($errors->any())
                    <div class="col-md-12">
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li class="ml-2">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Details</div>
                        <div class="card-body">
                            <a href="{{ url('/profile') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <br />
                            <br />

                            <form method="POST" action="{{ url('/profile/edit') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}

                                @include ('admin.form', ['formMode' => 'edit'])

                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Password</div>
                        <div class="card-body">
                            <form method="POST" action="{{ url('/profile/edit-password') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                                {{ method_field('PATCH') }}
                                {{ csrf_field() }}

                                @include ('admin.password-form', ['formMode' => 'edit-password'])

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
