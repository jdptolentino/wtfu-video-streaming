@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Reports</h1>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="row px-2">
                @if(Session::has('flash_message'))
                <div class="col-lg-12">
                    <div class="alert alert-success px-2">
                        <span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                    </div>
                @endif
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link <?php if($tab == 'requests') { echo 'active'; } ?> " id="requests-tab" data-toggle="tab" href="#requests" role="tab" aria-controls="requests" aria-selected="true">Requests</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link <?php if($tab == 'users') { echo 'active'; } ?> " id="users-tab" data-toggle="tab" href="#users" role="tab" aria-controls="users" aria-selected="true">Users</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="myTabContent">
                        <!-- requests -->
                        <div class="tab-pane fade show <?php if($tab == 'requests') { echo 'active'; } ?> " id="requests" role="tabpanel" aria-labelledby="requests-tab">
                            <div class="card">
                                <div class="card-header pb-0 bg-light">
                                    <form method="POST" action="{{ url('/reports') }}" accept-charset="UTF-8" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <div class="row">
                                            <div class="col-auto">
                                                <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                                                    <label for="status" class="control-label">{{ 'Status' }}</label>
                                                    <select class="form-control" name="status" id="status">
                                                        <option <?php if($status == 'ACTIVE'){ echo 'selected'; }?> value="ACTIVE">ACTIVE</option>
                                                        <option <?php if($status == 'LAPSED'){ echo 'selected'; }?> value="LAPSED">LAPSED</option>
                                                        <option <?php if($status == 'REQUESTED'){ echo 'selected'; }?> value="REQUESTED">REQUESTED</option>
                                                        <option <?php if($status == 'CANCELLED'){ echo 'selected'; }?> value="CANCELLED">CANCELLED</option>
                                                    </select>
                                                    {!! $errors->first('status', '<p class="text-danger help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="form-group">
                                                    <label for="date_range" class="control-label">{{ 'Date range' }}</label>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="far fa-calendar-alt"></i>
                                                        </span>
                                                        </div>
                                                        <input type="text" class="form-control float-right daterange" id="date_range" name="date_range" value="{{ $date_range }}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
                                                    <div>
                                                        <button class="btn btn-secondary">Generate</button>
                                                        </form>
                                                        @if(count($subscription) > 0)
                                                            <form method="POST" action="{{ url('/export/requests') }}" accept-charset="UTF-8" class="float-right ml-2">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="status" value="{{ $status }}">
                                                            <input type="hidden" name="date_range" value="{{ $date_range }}">
                                                            <button class="btn btn-success">Download</button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover text-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Username</th><th>Email</th><th>Subscription</th><th>Amount</th><th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($subscription) == 0)
                                            <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                                        @else
                                            @foreach($subscription as $item)
                                                <tr>
                                                    <!-- <td>{{ $item->id }}</td> -->
                                                    <td>{{ $item->user['user_name'] }}</td><td>{{ $item->user['email'] }}</td><td>{{ $item->plan->title }}</td><td>{{ number_format($item->plan->amount,2) }}</td>
                                                    <td>
                                                        <span class="badge bg-warning">{{ $item->status }}</span>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                    @if(count($subscription) > 0)
                                        <div class="pagination-wrapper px-3"> {!! $subscription->appends(['search' => Request::get('search')])->render() !!} </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <!-- users -->
                        <div class="tab-pane fade show <?php if($tab == 'users') { echo 'active'; } ?> " id="users" role="tabpanel" aria-labelledby="users-tab">
                            <div class="card">
                                <div class="card-header pb-0 bg-light">
                                    <form method="POST" action="{{ url('/reports') }}" accept-charset="UTF-8" class="form-horizontal">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="tab" value="users">
                                        <div class="row">
                                            <div class="col-auto">
                                                <div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
                                                    <label for="status" class="control-label">{{ 'Status' }}</label>
                                                    <select class="form-control" name="status" id="status">
                                                        <option <?php if($status == 'ACTIVE'){ echo 'selected'; }?> value="ACTIVE">ACTIVE</option>
                                                        <option <?php if($status == 'DELETED'){ echo 'selected'; }?> value="DELETED">DELETED</option>
                                                        <option <?php if($status == 'SUSPEND'){ echo 'selected'; }?> value="SUSPEND">SUSPEND</option>
                                                        <option <?php if($status == 'EMAIL_VERIFICATION'){ echo 'selected'; }?> value="EMAIL_VERIFICATION">EMAIL_VERIFICATION</option>
                                                    </select>
                                                    {!! $errors->first('status', '<p class="text-danger help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="form-group">
                                                    <label for="">&nbsp;</label>
                                                    <div>
                                                        <button class="btn btn-secondary">Generate</button>
                                                        </form>
                                                        @if(count($subscription) > 0)
                                                            <form method="POST" action="{{ url('/export/users') }}" accept-charset="UTF-8" class="float-right ml-2">
                                                            {{ csrf_field() }}
                                                            <input type="hidden" name="tab" value="users">
                                                            <input type="hidden" name="status" value="{{ $status }}">
                                                            <button class="btn btn-success">Download</button>
                                                            </form>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                                <div class="card-body table-responsive p-0">
                                    <table class="table table-hover text-nowrap">
                                        <thead>
                                            <tr>
                                                <th>Name</th><th>Email</th><th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @if(count($user) == 0)
                                            <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                                        @else
                                            @foreach($user as $item)
                                                <tr>
                                                    <td>{{ $item->full_name}}</td><td>{{ $item->email }}</td><td>{{ $item->status }}</td>
                                                    <td>
                                                        @if ($item->status === 'ACTIVE')
                                                        <span class="badge bg-primary">{{ $item->status }}</span>
                                                        @elseif ($item->status === 'DELETED')
                                                        <span class="badge bg-danger">{{ $item->status }}</span>
                                                        @elseif ($item->status === 'SUSPEND')
                                                        <span class="badge bg-warning">{{ $item->status }}</span>
                                                        @elseif ($item->status === 'EMAIL_VERIFICATION')
                                                        <span class="badge bg-success">{{ $item->status }}</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                    @if(count($user) > 0)
                                        <div class="pagination-wrapper px-3"> {!! $user->appends(['search' => Request::get('search'),'tab' => 'users'])->render() !!} </div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    
                </div>
            </div>
        </section>
    </div>
@endsection
@push('custom-scripts')
<script>
$(document).ready(function () {  
    var start = moment().subtract(29, 'days');
    var end = moment();

    function cb(start, end) {
        $('.daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }

    $('.daterange').daterangepicker({
        // startDate: start,
        // endDate: end,
        "opens": "right",
        ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
   }, cb);

//    cb(start, end);
});
</script>
@endpush
