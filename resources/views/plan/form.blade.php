<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($plan->title) ? $plan->title : old('title')}}" >
    {!! $errors->first('title', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('amount') ? 'has-error' : ''}}">
    <label for="amount" class="control-label">{{ 'Amount' }}</label>
    <input class="form-control" name="amount" type="number" id="amount" value="{{ isset($plan->amount) ? $plan->amount : old('amount')}}" >
    {!! $errors->first('amount', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($plan->description) ? $plan->description : old('description')}}</textarea>
    {!! $errors->first('description', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('frequency') ? 'has-error' : ''}}">
    <label for="frequency" class="control-label">{{ 'Frequency' }}</label>
    <input class="form-control" name="frequency" type="number" id="frequency" value="{{ isset($plan->frequency) ? $plan->frequency : old('frequency')}}" >
    {!! $errors->first('frequency', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('frequency_type') ? 'has-error' : ''}}">
    <label for="frequency_type" class="control-label">{{ 'Frequency Type' }}</label>
    <select name="frequency_type" class="form-control" id="frequency_type" >
        @foreach (json_decode('{"DAY": "Day", "MONTH": "Month", "YEAR": "Year"}', true) as $optionKey => $optionValue)
            <option value="{{ $optionKey }}" {{ (isset($plan->frequency_type) && $plan->frequency_type == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
        @endforeach
    </select>
    {!! $errors->first('frequency_type', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="control-label">{{ 'Status' }}</label>
    <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"ENABLED": "Enabled", "DISABLED": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($plan->status) && $plan->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="text-danger help-block">:message</p>') !!}
</div>

<div class="form-group {{ $errors->has('payment_instruction') ? 'has-error' : ''}}">
    <label for="payment_instruction" class="control-label">{{ 'Payment Instruction' }}</label>
    <textarea class="form-control" rows="5" name="payment_instruction" type="textarea" id="payment_instruction" >{{ isset($plan->payment_instruction) ? $plan->payment_instruction : old('payment_instruction')}}</textarea>
    {!! $errors->first('payment_instruction', '<p class="text-danger help-block">:message</p>') !!}
</div>

<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
