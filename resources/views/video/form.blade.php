<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="control-label">{{ 'Title' }}</label>
    <input class="form-control" name="title" type="text" id="title" value="{{ isset($video->title) ? $video->title : ''}}" >
    {!! $errors->first('title', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('link') ? 'has-error' : ''}}">
    <label for="link" class="control-label">{{ 'Link' }}</label>
    <textarea class="form-control" rows="5" name="link" type="textarea" id="link" >{{ isset($video->link) ? $video->link : ''}}</textarea>
    {!! $errors->first('link', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('isEmbed') ? 'has-error' : ''}}">
    <input name="isEmbed" type="checkbox" id="title" {{ isset($video->isEmbed) && $video->isEmbed == 1  ? "checked" : ''}} value="{{ isset($video->isEmbed) && $video->isEmbed == 1 ? 1 : 0}}">&nbsp;
    <label for="isEmbed" class="control-label">{{ 'isEmbed' }}</label>
    <br>
    <em>Note: if embed, change height to <b>245</b> and width to <b>100%</b></em>

</div>
<div class="form-group {{ $errors->has('description') ? 'has-error' : ''}}">
    <label for="description" class="control-label">{{ 'Description' }}</label>
    <textarea class="form-control" rows="5" name="description" type="textarea" id="description" >{{ isset($video->description) ? $video->description : ''}}</textarea>
    {!! $errors->first('description', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('category') ? 'has-error' : ''}}">
    <label for="category" class="control-label">{{ 'Category' }}</label>
    <select class="form-control" name="category_id" id="category">
        @if($categories)
            @foreach ($categories as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
            @endforeach
        @endif
    </select>
    {!! $errors->first('category', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="control-label">{{ 'Type' }}</label>
    <select class="form-control" name="type" id="type">
        <option value="FREE" {{ isset($video->type) && $video->type === 'FREE' ? 'selected' : '' }}>FREE</option>
        <option value="PAID" {{ isset($video->type) && $video->type === 'PAID' ? 'selected' : '' }}>PAID</option>
    </select>
    {!! $errors->first('type', '<p class="text-danger help-block">:message</p>') !!}
</div>
<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    <label for="photo" class="control-label">{{ 'photo' }}</label>
    <input class="form-control" name="photo" type="file" id="title" value="" >
</div>
<div class="form-group">
    <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
</div>
