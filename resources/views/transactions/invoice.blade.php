@extends('layouts.app')
<header>
    <style>
        @media print {
            .myDivToPrint {
                background-color: white;
                height: 100%;
                width: 100%;
                position: fixed;
                top: 0;
                left: 0;
                margin: 0;
                padding: 15px;
                font-size: 14px;
                line-height: 18px;
            }
            .myDivToPrint button {
                display: none;
            }

            .card-header{
                display: none;
            }
        }
    </style>
</header>
@section('content')
    <div class="content-wrapper">
        <!-- <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Transaction {{ $transaction->id }}</h1>
                    </div>
                </div> 
            </div>
        </div>

        <section class="content myDivToPrint">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">

                            <a href="{{ url('/transactions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>Date</th><td>{{ date('d F Y', strtotime($transaction->created_at)) }}</td>
                                        </tr>
                                        <tr>
                                            <th>Transaction Number</th><td>{{ $transaction->id }}</td>
                                        </tr>
                                        <tr>
                                            <th>Payment Method</th><td>Card <\Card number></td>
                                        </tr>
                                        <tr>
                                            <th>Product</th><td>Web Video Subscription</td>
                                        </tr>  
                                        <tr>
                                            <th>Total Tax</th><td>0</td>
                                        </tr> 
                                        <tr>
                                        <th>Total</th><td>{{ $transaction->amount}}</td>
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section> -->
        <br>
        <section class="content mt-n2">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Invoice Details</h3>
                            <div class="card-tools">
                                <a href="{{ url('/transactions') }}" title="Back"><button class="btn btn-warning btn-sm mt-n2 mb-n1"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                                <button type="button" class="btn btn-success btn-sm mt-n2 mb-n1" onclick="javascript:window.print();">
                                    <i class="fa fa-print"></i> Print Invoice
                                </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 mb-5">
                                    <div class="float-left text-left">Trans.#: {{ $transaction->id }}</div>
                                    <div class="float-right text-right">Trans. Date: {{ date('d F Y', strtotime($transaction->created_at)) }}</div>
                                    </h6>
                                </div>
                            </div>
                            <div class="row my-8">
                                <div class="col-6">
                                    <p class="h3">{{ config('app.name', 'Video Stream') }}</p>
                                    <address>
                                        sample@email.com
                                    </address>
                                </div>
                                <div class="col-6 text-right">
                                    <p class="h3">{{ $transaction->user->user_name }}</p>
                                    <address>
                                        {{ $transaction->user->email }}
                                    </address>
                                </div>
                            </div>
                            
                            <div class="table-responsive push">
                                <table class="table table-bordered text-nowrap">
                                    <tr>
                                        <th style="width:15%">Product</th>
                                        <th class="text-center" style="width: 1%">Qnt</th>
                                        <th class="text-right" style="width: 1%">Amount</th>
                                    </tr>
                                    
                                    <tr>
                                        <td>
                                            <p class="font-w600 mb-1">{{ $transaction->plan->title }}</p>
                                            <span class="text-muted small">{{ $transaction->plan->description }}</span>
                                        </td>
                                        <td class="text-center">1</td>
                                        <td class="text-right">{{ number_format($transaction->amount,2) }}</td>
                                        
                                    </tr>
                                    <!-- <tr>
                                        <td colspan="4" class="font-w600 text-right">Subtotal</td>
                                        <td class="text-right">$25.000,00</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Vat Rate</td>
                                        <td class="text-right">20%</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="font-w600 text-right">Vat Due</td>
                                        <td class="text-right">$5.000,00</td>
                                    </tr> -->
                                    <tr>
                                        <td colspan="2" class="font-weight-bold text-uppercase text-right">Total Due</td>
                                        <td class="font-weight-bold text-right">{{ number_format($transaction->amount,2) }}</td>
                                    </tr>
                                </table>
                            </div>
                            <p class="text-muted text-center">Thank you very much for doing business with us. We look forward to working with you again!</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
