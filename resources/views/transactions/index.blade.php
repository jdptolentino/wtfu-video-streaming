@extends('layouts.app')

@section('content')

    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Transactions</h1>
                    </div>
                </div>
            </div>
        </div>
        
        <section class="content">
            <div class="row px-2">
                @if(Session::has('flash_message'))
                <div class="col-lg-12">
                    <div class="alert alert-success px-2">
                        <span class="glyphicon glyphicon-ok"></span><em> {!! session('flash_message') !!}</em></div>
                    </div>
                @endif
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            {{-- <a href="{{ url('/transactions/create') }}" title="Back"><button class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> Add New</button></a> --}}
                            <div class="card-tools">
                                <form method="GET" action="{{ url('/transactions') }}">
                                    <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search" value="{{ request('table_search') ? request('table_search') : '' }}">
                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="card-body table-responsive p-0">
                            <table class="table table-hover text-nowrap">
                                <thead>
                                    <tr>
                                        <th>Username</th><th>Email</th><th>Subscription</th><th>Amount</th><th>Status</th><th width="100">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if(count($transactions) == 0)
                                    <tr><td colspan="100%"><div class="alert alert-danger">No data found</div></td></tr>
                                @else
                                    @foreach($transactions as $item)
                                        <tr>
                                            <!-- <td>{{ $item->id }}</td> -->
                                            <td>{{ $item->user->user_name }}</td><td>{{ $item->user->email }}</td><td>{{ $item->plan->title }}</td><td>{{ number_format($item->amount,2) }}</td>
                                            <td>
                                                @if ($item->status === 'PAID')
                                                <span class="badge bg-success">{{ $item->status }}</span>
                                                @elseif ($item->status === 'FAILED')
                                                <span class="badge bg-black">{{ $item->status }}</span>
                                                @elseif ($item->status === 'DECLINED')
                                                <span class="badge bg-danger">{{ $item->status }}</span>
                                                @elseif ($item->status === 'PENDING')
                                                <span class="badge bg-warning">{{ $item->status }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="{{ url('/transactions/invoice/' . $item->id) }}" title="View Invoice"><button class="btn btn-success btn-sm"><i class="fa fa-file" aria-hidden="true"></i> Invoice</button></a>
                                                <a href="{{ url('/transactions/' . $item->id) }}" title="View transaction"><button class="btn btn-info btn-sm"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                                                {{-- <a href="{{ url('/transactions/' . $item->id . '/edit') }}" title="Edit transaction"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a> --}}
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            @if(count($transactions) > 0)
                                <div class="pagination-wrapper px-3"> {!! $transactions->appends(['search' => Request::get('search')])->render() !!} </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection
