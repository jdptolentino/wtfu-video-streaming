@extends('layouts.app')

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">View Transaction</h1>
                    </div>
                </div> 
            </div>
        </div>

        <section class="content">
            <div class="row px-2">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-header">Transaction# {{ $transaction->id }}</div>
                        <div class="card-body">

                            <a href="{{ url('/transactions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                            {{-- <a href="{{ url('/transactions/' . $transaction->id . '/edit') }}" title="Edit transaction"><button class="btn btn-primary btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> Edit</button></a> --}}
                            <a href="{{ url('/transactions/invoice/' . $transaction->id) }}" title="View Invoice"><button class="btn btn-success btn-sm"><i class="fa fa-file" aria-hidden="true"></i> Invoice</button></a>
                            <form method="POST" action="{{ url('transactions' . '/' . $transaction->id) }}" accept-charset="UTF-8" style="display:inline">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-danger btn-sm" title="Delete transaction" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash" aria-hidden="true"></i> Delete</button>
                            </form>
                            <br/>
                            <br/>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr>
                                            <th>TXN ID</th><td>{{ $transaction->id }}</td>
                                        </tr>
                                        <tr><th> Username </th><td> {{ $transaction->user->user_name }} </td></tr><tr><th> Subscription </th><td> {{ $transaction->plan->title }} </td></tr><tr><th> Amount </th><td> {{ number_format($transaction->amount,2) }} </td></tr><tr><th> Status </th><td> {{ $transaction->status }} </td></tr><tr><th> Paymen type </th><td> Credit Card </td></tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
