<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(AdminsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(VideosTableSeeder::class);
        $this->call(SubscriptionPlanSeeder::class);
        $this->call(SubscriptionSeeder::class);
        $this->call(TransactionsSeeder::class);
        $this->call(SeriesTableSeeder::class);
        $this->call(SeriesVideosTableSeeder::class);
    }
}
