<?php

use Illuminate\Database\Seeder;

class SeriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('series')->insert([
            'title' => 'All in One Fun',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'photo' => '',
            'slug' => str_replace(' ', '-', strtolower('All in One Fun')),
            'status' => 'ENABLED',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);
    }
}
