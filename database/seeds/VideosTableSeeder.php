<?php

use Illuminate\Database\Seeder;

class VideosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $videos = [146599806, 96559752, 35060098, 29986424, 24367351, 47376460, 126777001, 122875681, 24374425];
        $data = array_map(function($item) {
            return [
                'title' => 'Digital Nature ' . rand(1000, 9000),
                'link' => 'https://vimeo.com/' . $item,
                'description' => 'Part of the "SCIENCE & NATURE" Exhibition coming to Gallery 1988 December 4th',
                'category_id' => 1,
                'created_at' => date('Y-m-d'),
                'updated_at' => date('Y-m-d')
            ];

        }, $videos);

        DB::table('videos')->insert($data);


    }
}
