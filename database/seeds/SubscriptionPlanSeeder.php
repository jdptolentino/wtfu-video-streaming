<?php

use Illuminate\Database\Seeder;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subscription_plans')->insert([
            'title' => '1 Month All Access',
            'amount' => '100',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'frequency'  => 1,
            'status' => 'ENABLED',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);

        DB::table('subscription_plans')->insert([
            'title' => '3 Months All Access',
            'amount' => '200',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit.',
            'frequency' => 3,
            'status' => 'ENABLED',
            'created_at' => date('Y-m-d'),
            'updated_at' => date('Y-m-d')
        ]);
    }
}
