<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSubscriptionStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions', function($table) {
            $table->dropColumn('status');
        });

        Schema::table('subscriptions', function($table) {
            $table->enum('status', ['ACTIVE', 'LAPSED', 'CANCELLED', 'REQUESTED'])->default('REQUESTED')->after('plan_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('subscriptions', function($table) {
            $table->dropColumn('status');
        });

        Schema::table('subscriptions', function($table) {
            $table->enum('status', ['ACTIVE', 'LAPSED', 'CANCELLED'])->default('ACTIVE')->after('plan_id');
        });
    }
}
