<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SeriesTest extends TestCase
{
    private $token = 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImRiNTZjYjZhM2M2NzMzZDJiYjI2YjMwNTM2Nzk2Yzk5OWE0MjA4MTYzZGJmOGYyMDNlZmMzNTE3NGYxZjQ3ZjhmMDNhMjc5MGYyN2VhNTk3In0.eyJhdWQiOiIxIiwianRpIjoiZGI1NmNiNmEzYzY3MzNkMmJiMjZiMzA1MzY3OTZjOTk5YTQyMDgxNjNkYmY4ZjIwM2VmYzM1MTc0ZjFmNDdmOGYwM2EyNzkwZjI3ZWE1OTciLCJpYXQiOjE1ODMxNDM3OTQsIm5iZiI6MTU4MzE0Mzc5NCwiZXhwIjoxNjE0Njc5Nzk0LCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.RJ3xpykMShKIj-WUspjYdEFc6frbG54N0XXZaJRIKv9Eo7MCr1hAnDGL-dsrn5fDeCaH4XinQJNJSV7SyEK9YKFX6y9QWtKnwlOLfNpacwMF4yec89oQanxF2YePa-oAz9Wjvox8iKRSzdBQZPUqEARmrPeuuaoWWNOr780FY-H2ewG6wG7QOdA0m3CYvfLthmY4wYvGK5EIkkwj5ZgupPPlwxL1VLOrfjhUVGY1gGkwT9xY4QO0t1gfc-veyrpU7l_HrHhZLf0n1wfD9ujWKr_sLnT_bgCdjDhBYEr_3YuaomTxrSBFC7gUS3Y9SwAgj-kFN_sRNEkEj37CJpIf2f8oA5C7-DPWZhYdHNSeZb91Aqb_oPOXGa7Yh9ewL4RiD47_HLLW4S3Th-eN9LTqUBzQXn9hS320_DAt7tYjyhJk-mlfspzkB5HfMc_Rf9CWPZDwvIyICdqfxXh7jfgVaCin_zuUPn0L82hvjhd-T_YaT-4RCePfredtclvzioFVzK3x3e5njfrpng9LkiEWGB9HuX86NJbAj67-RIuZ-FrpMRcBDcz0pPoHxanRDbGLU9kAZgThBP5yG2yyEYjfhOcxVCYiSiCTjP8XrOWrpNvsvzL8vi7iuCdh2dd8o3ujtPgwxR06LcNcyK6C0fE5QxHTmIPMf5GKSRb4ll6vP-8';

    /**
     * Test Get All series
     *
     * @return void
     */
    public function testGetAll()
    {
        $this->withoutExceptionHandling();

        $response = $this->withHeaders([
            'Authorization' => $this->token,
            'Accept' => 'application/json'
        ])->json('GET', '/api/series?perPage=2&page=1');

        $response->assertOk();
        $this->assertCount(1, \App\Models\Series::all());
    }

    /**
     * Test get series by slug
     *
     * @return void
     */

     public function testGetBySlug() {
         $this->withoutExceptionHandling();

         $response = $this->withHeaders([
            'Authorization' => $this->token,
            'Accept' => 'application/json'
        ])->json('GET', '/api/series/all-in-one-fun');

        $response->assertOk();
        $this->assertCount(1, \App\Models\Series::where('slug', 'all-in-one-fun')->get());
     }
}
